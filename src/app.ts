import express from "express"
import mongoose from "mongoose"
import bodyParser from "body-parser";
 import { ProductRoutes } from "../src/routes/ProductRoutes";
 import { OrderRoutes } from "../src/routes/OrderRoutes";

// Connect to MongoDB database
mongoose.connect("mongodb+srv://muzakkar066:api066@cluster0.4ueal.mongodb.net/test?authSource=admin&replicaSet=atlas-30r2ig-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true", { useNewUrlParser: true ,  useUnifiedTopology: true })
    .then(() => {
        const app = express()
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(ProductRoutes)
        app.use(OrderRoutes)
        app.listen(5000, () => {
            console.log("Server has started!")
        })
    })
