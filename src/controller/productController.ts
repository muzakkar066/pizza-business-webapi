import { ProductTable } from '../schema/product';
export class ProductController {
    constructor() { }
    getProduct(_id: string) {
        return ProductTable.findById(_id);
    }
    SaveProduct(product: any) {
        return new ProductTable(product).save();
    }
    updateProduct(product: any) {
        return ProductTable.findByIdAndUpdate(product._id, product, {
            new: true
        });
    }
    deleteProduct(_id: string) {
        return ProductTable.findByIdAndDelete(_id);
    }
    getProductList() {
        return ProductTable.find();
    }
}