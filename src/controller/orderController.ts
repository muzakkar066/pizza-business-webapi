import { OrderTable } from '../schema/order';
export class orderController {
    constructor() { }
    getOrder(_id: string) {
        return OrderTable.findById(_id);
    }
    SaveOrder(order: any) {
        return new OrderTable(order).save();
    }
    updateOrder(order: any) {
        return OrderTable.findByIdAndUpdate(order._id, order, {
            new: true
        });
    }
    deleteOrder(_id: string) {
        return OrderTable.findByIdAndDelete(_id);
    }
    getOrdersList() {
        return OrderTable.find();
    }
}