import express from 'express';
import { ProductController } from '../controller/productController';

export class ProductsPL {
    router: express.Router;
    constructor() {
        this.router = express.Router();
        this.routes();
    }
    routes() {
        //retruns all products
        this.router.get('/', async (req, res, next) => {
            new ProductController()
                .getProductList()
                .then((products: any) => {
                    if (!products) {
                        console.error(404, 'No products found');
                    }
                    res.status(200).json({
                        products
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });
        //save a product
        this.router.post('/postProduct', async (req, res, next) => {
            new ProductController()
                .SaveProduct(req.body.product)
                .then((product: any) => {
                    if (!product) {
                        console.error(404, 'product not saved');
                    }
                    res.status(200).json({
                        product
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });
        //update a product
        this.router.put('/productUpdate', async (req, res, next) => {
            new ProductController()
                .updateProduct(req.body.product)
                .then((product: any) => {
                    if (!product) {
                        console.error(404, 'product not updated');
                    }
                    res.status(200).json({
                        product
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });
        // get one product
        this.router.get('/productOne', async (req, res, next) => {
            new ProductController()
                .getProduct(req.body.id)
                .then((product: any) => {
                    if (!product) {
                        console.error(404, 'No product found');
                    }
                    res.status(200).json({
                        product
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });
        //delete a product
        this.router.delete('/productdel', async (req, res, next) => {
            new ProductController()
                .deleteProduct(req.body.id)
                .then((product: any) => {
                    if (!product) {
                        console.error(404, 'No products found');
                    }
                    res.status(200).json({
                        message:"Product deleted Successfully"
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });
    }
}
export const ProductRoutes = new ProductsPL().router;