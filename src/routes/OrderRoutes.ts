import express from 'express';
import { orderController } from '../controller/orderController';

export class Order {
    router: express.Router;
    constructor() {
        this.router = express.Router();
        this.routes();
    }
    routes() {

        //Get all orders of different Products
        this.router.get('/OrderList', async (req, res, next) => {
            new orderController()
                .getOrdersList()
                .then((orders: any) => {
                    if (!orders) {
                        console.error(404, 'No orders found');
                    }
                    res.status(200).json({
                        orders
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });
                //Post orders on different Products
                this.router.post('/postOrder', async (req, res, next) => {
                    new orderController()
                        .SaveOrder(req.body.order)
                        .then((orders: any) => {
                            if (!orders) {
                                console.error(404, 'Order not placed');
                            }
                            res.status(200).json({
                                orders
                            });
                        })
                        .catch((err: Error) => {
                            console.log(err);
                        });
                });

                  //Update Orders in shopping card
            this.router.put('/orderUpdate', async (req, res, next) => {
            new orderController()
                .updateOrder(req.body.order)
                .then((orders: any) => {
                    if (!orders) {
                        console.error(404, 'orders not updated');
                    }
                    res.status(200).json({
                        orders
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });

                    // get one order by order id
        this.router.get('/oderId', async (req, res, next) => {
            new orderController()
                .getOrder(req.body.id)
                .then((product: any) => {
                    if (!product) {
                        console.error(404, 'No product found');
                    }
                    res.status(200).json({
                        product
                    });
                })
                .catch((err: Error) => {
                    console.log(err);
                });
        });
               // Delete Order By order id
               this.router.delete('/orderIdDel', async (req, res, next) => {
                new orderController()
                    .deleteOrder(req.body.id)
                    .then((orders: any) => {
                        if (!orders) {
                            console.error(404, 'No order found');
                        }
                        res.status(200).json({
                            message:"Order deleted Successfully"
                        });
                    })
                    .catch((err: Error) => {
                        console.log(err);
                    });
            });
        
    }
}
export const OrderRoutes = new Order().router;