import { Schema, model } from 'mongoose';
const OrderSchema = new Schema(
    {
        orderQty: { 
            type: Number,
            required:true
            },
        productId:{
                   type: Schema.Types.ObjectId,
                   ref: 'PrductSchema',
                   required:true
            },

        orderStatus: {
             type: String,
             required: true,
             default: "inProcess"
          },

        orderAmount:{
            type:Number,
            required: true
        },

        totalAmount:{
            type:Number,
            required:true
        }
    },

    { timestamps: true }
);
export const OrderTable = model('OrderSchema', OrderSchema);