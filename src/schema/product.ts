import { Schema, model } from 'mongoose';
const ProductSchema = new Schema(
    {   
        VendorId:  { type: Number},
        PizzaName: { type: String },
        PizzaDescrpation: { type: String },
        PizzaPrice: { type: Number }
    },
    { timestamps: true }
);
export const ProductTable = model('PrductSchema', ProductSchema);